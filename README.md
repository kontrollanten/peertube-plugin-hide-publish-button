# PeerTube plugin for hiding the publish button

The plugin checks the current video quota and hides the button if it's set to zero.
